from scrapy.crawler import CrawlerProcess
from bustednewspaper.spiders.bustednews import BustednewsSpider
from scrapy.utils.project import get_project_settings

from tkinter import Tk, Label, Button, StringVar, OptionMenu, IntVar, filedialog
import tkinter as tk
import os


class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("Scraper GUI")

        # Create the basic Elements
        self.label = Label(master, text="Welcome to Bustednewspaper scraper ... It Rhymes!")
        self.start_button = Button(master, text="Start", command=self.start_spider)
        self.close_button = Button(master, text="Close", command=self.exit_spider)
        self.dir_button = Button(master, text="Choose Directory", command=self.update_dir)

        self.state_list_options = StringVar(master)
        self.state_list = OptionMenu(master, self.state_list_options, [])
        self.stateChosenFlag = IntVar()
        self.confirm_state_button = Button(master, text="Confirm State", command=lambda: self.stateChosenFlag.set(1))

        self.county_list_options = StringVar(master)
        self.county_list = OptionMenu(master, self.county_list_options, [])
        self.countyChosenFlag = IntVar()
        self.confirm_county_button = Button(master, text="Confirm County", command=lambda: self.countyChosenFlag.set(1))

        # Style the elements
        self.label.grid(columnspan=3, rowspan=2)
        self.start_button.grid(columnspan=3)
        self.dir_button.grid(columnspan=3)
        self.state_list.grid(columnspan=2)
        self.confirm_state_button.grid(row=4, column=2)
        self.county_list.grid(columnspan=2)
        self.confirm_county_button.grid(row=5, column=2)
        self.close_button.grid(columnspan=3)

    def update_dir(self):
        dir = filedialog.askdirectory(initialdir=".")
        os.chdir(dir)

    def start_spider(self):
        self.process = CrawlerProcess(get_project_settings())
        self.process.crawl(BustednewsSpider, myGui=my_gui)
        self.process.start()

    def exit_spider(self):
        self.process.stop()
        self.master.quit()

    def update_state_list(self, items):
        # Reset state_list_options and delete all old options
        self.state_list_options.set('')
        self.state_list['menu'].delete(0, 'end')

        # Insert list of new options (tk._setit hooks them up to self.state_list_options)
        for item in items:
            self.state_list['menu'].add_command(label=item, command=tk._setit(self.state_list_options, item))

        # Wait for button press to confirm choice
        print("[log] Waiting for state selection ...")
        self.confirm_state_button.wait_variable(self.stateChosenFlag)

        # Once selection is confirmed resume the spider
        chosenState = self.state_list_options.get()
        print("[log] Chosen State is: " + chosenState)
        return items.index(chosenState)

    def update_county_list(self, items):
        # Reset county_list_options and delete all old options
        self.county_list_options.set('')
        self.county_list['menu'].delete(0, 'end')

        # Insert list of new options (tk._setit hooks them up to self.county_list_options)
        for item in items:
            self.county_list['menu'].add_command(label=item, command=tk._setit(self.county_list_options, item))

        # Wait for button press to confirm choice
        print("[log] Waiting for county selection ...")
        self.confirm_county_button.wait_variable(self.countyChosenFlag)

        # Once selection is confirmed resume the spider
        chosenCounty = self.county_list_options.get()
        print("[log] Chosen County is: " + chosenCounty)
        return items.index(chosenCounty)


root = Tk()
my_gui = MyFirstGUI(root)
root.mainloop()
