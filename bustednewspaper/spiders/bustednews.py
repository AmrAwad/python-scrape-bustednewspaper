# -*- coding: utf-8 -*-
import scrapy
import urllib
import PIL
from PIL import Image


def resize_image_template(fileName):
    image = PIL.Image.open(fileName)
    image = image.resize((480, 600), PIL.Image.LANCZOS)
    image.save(fileName)


def download_image_from_url(url, imageName):
    urllib.request.urlretrieve(url, imageName)


class BustednewsSpider(scrapy.Spider):
    name = 'bustednews'
    start_urls = ['https://bustednewspaper.com/']

    def __init__(self, myGui=None):
        self.myGui = myGui

    def parse(self, response):
        # Get complete list of names and links of each state
        statesList = response.css('a[href$="-mugshots/"]')
        stateNamesList, stateLinksList = self.get_names_links(statesList)

        if self.myGui is not None:
            # Print list to gui & get choice from index
            index = self.myGui.update_state_list(stateNamesList)
        else:
            # if No Gui is available
            # Print Available States
            print("[log] Available States: ")
            self.print_items_list(stateNamesList)
            # Prompt user to choose from available states
            index = self.input_from_list(stateNamesList)

        # Create a request for the chosen state
        yield scrapy.Request(url=stateLinksList[index], callback=self.parse_state)

    def parse_state(self, response):
        # Get complete list of names and links of each county
        countiesList = response.css("div.main-content").css("ul > li")
        countyNamesList, countyLinksList = self.get_names_links(countiesList)

        if self.myGui is not None:
            # Print list to gui & get choice from index
            index = self.myGui.update_county_list(countyNamesList)
        else:
            # if No Gui is available
            # Print Available Counties
            print("[log] Available Counties: ")
            self.print_items_list(countyNamesList)
            # Prompt user to choose from available states
            index = self.input_from_list(countyNamesList)

        # Create a request for the chosen county
        yield scrapy.Request(url=countyLinksList[index], callback=self.parse_county_firstPage)

    def parse_county_firstPage(self, response):
        # Get total Number of Pages
        TotalPagesNum = int(response.css("a.page-numbers::text").extract()[-1].replace(",", ""))
        print("[log] Total Number of Pages: " + str(TotalPagesNum))

        print("[log] Parsing Page #: 1")

        print("[log] parsing listing...")
        mugshots = response.css("div.listing").css("article.highlights")

        for mugshot in mugshots:
            pageUrl = mugshot.css("a.image-link::attr(href)").extract_first()
            yield scrapy.Request(url=pageUrl, callback=self.parse_people)

        # Getting Url for next page
        nextPageUrl = response.css("a.next::attr(href)").extract_first()
        if nextPageUrl:
            yield scrapy.Request(url=nextPageUrl, callback=self.parse_county)

    def parse_county(self, response):
        pageNum = response.url.split("/")[-2]
        print("[log] Parsing Page #: " + str(pageNum))

        print("[log] parsing listing...")
        mugshots = response.css("div.listing").css("article.highlights")

        for mugshot in mugshots:
            pageUrl = mugshot.css("a.image-link::attr(href)").extract_first()
            yield scrapy.Request(url=pageUrl, callback=self.parse_people)

        # Getting Url for next page
        nextPageUrl = response.css("a.next::attr(href)").extract_first()
        if nextPageUrl:
            yield scrapy.Request(url=nextPageUrl, callback=self.parse_county)

    def parse_people(self, response):
        print("[log] Parsing person ... ")
        tables = response.css("div.post-content-right").css("table")
        for table in tables:
            if table.re("charge description") != []:
                charge = table.css("table > tr")[1].css("td::text").extract_first()
            elif table.re("name") != []:
                name = table.css("table > tr")[0].css("td::text").extract_first()

        imageUrl = response.css("div.featured > a::attr(href)").extract_first()
        fileName = name + ' - ' + charge.replace("/", " ").replace("\\", " ") + '.jpg'
        download_image_from_url(imageUrl, fileName)
        resize_image_template(fileName)

    def input_from_list(self, choiceList):
        while True:
            try:
                index = int(input("[input] Please Choose an index:"))
            except ValueError:
                print("[Error] Invalid Number!")
                continue
            try:
                choice = choiceList[index]
            except IndexError:
                print("[Error] Index is out of range!")
                continue
            print("[log] Chosen: ", choice)
            break
        return index

    def get_names_links(self, items):
        namesList = []
        linksList = []
        for item in items:
            name = item.css("a::text").extract_first()
            namesList.append(name)
            link = item.css("a::attr(href)").extract_first()
            linksList.append(link)
        return namesList, linksList

    def print_items_list(self, items):
        for index, value in enumerate(items):
            print("\t" + str(index) + ") " + value)
