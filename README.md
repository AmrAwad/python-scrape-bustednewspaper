### Overview ###

This repository contains the code used to scrape the website: [bustednewspaper](https://bustednewspaper.com/)

This project will use `scrapy`.

### Installation ###

The only package you need to install so far is `scrapy`, you can easily do this with this command:
```
pip install scrapy
```

### Running the spider ###

go to the directory `{project_dir}\bustednewspaper\spiders` and open a cmd in the current location, then run this command:
```
scrapy runspider bustednews.py
```
You should get a numbered list of all the states :D

### Notes ###
* if you want to see more details about what the spider is doing, you can comment line 17 in `settings.py` file and rerun the spider, you'll get many more log messages about what the spider is doing.
